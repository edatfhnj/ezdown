﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Reflection;
using UnityEngine;
using UnityEngine.UI;
using UnityModManagerNet;
using Harmony12;

namespace EzDown
{
    public class Setting : UnityModManager.ModSettings
    {
        public override void Save(UnityModManager.ModEntry modEntry)
        {
            base.Save(modEntry);
        }
    }
    class MainMod
    {/// <summary>
     /// 确定MOD是开启的
     /// </summary>
        public static bool enable;
        /// <summary>
        /// 玩家的设置文件
        /// </summary>
        public static Setting settings;
        /// <summary>
        /// 游戏中输出信息
        /// </summary>
        public static UnityModManager.ModEntry.ModLogger logger;

        public static playercon playercon;

        public static bool Load(UnityModManager.ModEntry modEntry)
        {
            //读取配置
            settings = Setting.Load<Setting>(modEntry);
            //存储日志
            logger = modEntry.Logger;

            //打开mod配置时
            //modEntry.OnGUI = OnGUI;
            //保存mod配置时
            modEntry.OnSaveGUI = OnSaveGUI;
            //开关mod时
            modEntry.OnToggle = OnToggle;

            //更改应用到游戏
            var harmony = HarmonyInstance.Create(modEntry.Info.Id);
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            playercon = PlayerStatus.Instance.objplayer.GetComponent<playercon>();
            return true;
        }

        private static bool OnToggle(UnityModManager.ModEntry arg1, bool arg2)
        {
            enable = arg2;
            return true;
        }

        private static void OnSaveGUI(UnityModManager.ModEntry obj)
        {
            settings.Save(obj);
        }


        [HarmonyPatch(typeof(playercon), "fun_damage")]
        public static class playercon_fun_damage_Postfix
        {
            public static void Postfix(playercon __instance)
            {
                if (enable)
                {
                    playercon.ImmediatelyERO();
                }

            }
        }
        
        [HarmonyPatch(typeof(playercon), "ItemGetDamage")]
        public static class playercon_ItemGetDamage_Postfix
        {
            public static void Postfix(playercon __instance)
            {
                if (enable)
                {
                    playercon.ImmediatelyERO();
                }

            }
        }
        [HarmonyPatch(typeof(playercon), "fun_damage_Improvement")]
        public static class playercon_fun_damage_Improvement_Postfix
        {
            public static void Postfix(playercon __instance)
            {
                if (enable)
                {
                    playercon.ImmediatelyERO();
                }

            }
        }
    }
}
